package com.sda.company.controller;

import com.sda.company.service.CompanyService;
import com.sda.company.util.CustomFakerCompany;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/company")
@ControllerAdvice
public class CompanyController {
    private final CompanyService companyService;
    private final CustomFakerCompany customFakerCompany;

    @Autowired
    public CompanyController(CompanyService companyService, CustomFakerCompany customFakerCompany) {
        this.companyService = companyService;
        this.customFakerCompany = customFakerCompany;
    }


    @GetMapping("/hello")
    public ResponseEntity<String> hello(){
        return ResponseEntity.ok("Hello World");
    }

    @GetMapping("/hello/{name}")
    public ResponseEntity<String> helloName(@PathVariable String name, Integer age){

        return ResponseEntity.ok("Hello " + name);
    }

    @GetMapping("/helloNameAndAge/{name}")
    public ResponseEntity<String> helloNameAndAge(@PathVariable String name, @RequestParam Integer age){

        return ResponseEntity.ok("Hello " + name + " age: " + age);
    }

    @PostMapping("/create")
    public ResponseEntity<CompanyDisplayDto> createCompany(@RequestBody @Valid CompanyCreateDto companyCreateDto){
        CompanyDisplayDto companyDisplayDto = companyService.createCompany(companyCreateDto);
     return ResponseEntity.ok(companyDisplayDto);
    }

    @GetMapping("/findByName/{name}")
    public ResponseEntity<CompanyDisplayDto> findByName(@PathVariable String name){
        CompanyDisplayDto companyDisplayDto = companyService.findByName(name);
        return ResponseEntity.ok(companyDisplayDto);
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<CompanySummaryDto>> findAll(@RequestParam(defaultValue = "0") Integer pageNumber,
                                                           @RequestParam(defaultValue = "10") Integer pageSize,
                                                           @RequestParam(defaultValue = "id") String sortBY){
        return ResponseEntity.ok(companyService.findAll(pageNumber,pageSize,sortBY));
    }

    @GetMapping("/generateCompanies")
    public ResponseEntity<String> generateCompanies(){
        companyService.saveAll(customFakerCompany.generateCompanies());

        return ResponseEntity.ok("Companies were generated");
    }

    @PutMapping("/update")
    public ResponseEntity<CompanyDisplayDto> updateCompany(@RequestBody @Valid CompanyUpdateDto companyUpdateDto){
        CompanyDisplayDto companyDisplayDto = companyService.updateCompany(companyUpdateDto);

        return ResponseEntity.ok(companyDisplayDto);

    }

    @DeleteMapping("/deleteById/{id}")
    public ResponseEntity<String> deleteCompany(@PathVariable Integer id){
        companyService.deleteById(id);

        return ResponseEntity.ok("Company Deleted successfully");
    }

}

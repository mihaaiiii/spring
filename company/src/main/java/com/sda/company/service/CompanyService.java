package com.sda.company.service;

import com.sda.company.model.Company;

import java.util.List;

public interface CompanyService {
    CompanyDisplayDto createCompany(CompanyCreateDto companyCreateDto);
    CompanyDisplayDto findByName(String name);

    List<CompanySummaryDto> findAll(Integer pageNumber, Integer pageSize, String sortBy);

     void saveAll(List<Company> companyList);

     CompanyDisplayDto updateCompany(CompanyUpdateDto companyUpdateDto);

     void  deleteById(Integer id);
}

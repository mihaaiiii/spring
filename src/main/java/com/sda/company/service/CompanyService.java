package com.sda.company.service;

import com.sda.company.dto.companydto.CompanyCreateDto;
import com.sda.company.dto.companydto.CompanyDisplayDto;
import com.sda.company.dto.companydto.CompanySummaryDto;
import com.sda.company.dto.companydto.CompanyUpdateDto;
import com.sda.company.model.Company;

import java.util.List;

public interface CompanyService {

    CompanyDisplayDto createCompany(CompanyCreateDto companyCreateDto);

    CompanyDisplayDto findByName(String companyName);

    List<CompanySummaryDto> findAll(Integer pageNumber, Integer pageSize, String sortBy);

    void saveAll(List<Company> companyList);

    CompanyDisplayDto updateCompany(CompanyUpdateDto companyUpdateDto);

    void deleteById(Integer id);

    Company findCompanyByName(String companyName);


}

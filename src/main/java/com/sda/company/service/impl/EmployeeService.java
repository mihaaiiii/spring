package com.sda.company.service.impl;

import com.sda.company.dto.employeDto.EmployeeCreateDto;
import com.sda.company.dto.employeDto.EmployeeDisplayDto;
import com.sda.company.dto.employeDto.EmployeeSummaryDto;
import com.sda.company.dto.employeDto.EmployeeUpdateDto;
import com.sda.company.model.employe.Employee;

import java.util.List;

public interface EmployeeService {

    EmployeeDisplayDto createEmployee(EmployeeCreateDto employeeCreateDto);

    EmployeeDisplayDto findByName(String employeeName);

    List<EmployeeSummaryDto> findAll();

    void saveAll(List<Employee> employeeList);

    EmployeeDisplayDto updateEmployee(EmployeeUpdateDto employeeUpdateDto);

    void deleteById(Integer employeeId);

    Employee findEmployeeByName(String employeeName);
}

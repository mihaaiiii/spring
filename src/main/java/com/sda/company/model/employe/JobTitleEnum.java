package com.sda.company.model.employe;

public enum JobTitleEnum {

    DEVELOPER,
    TESTER,
    TEAM_LEAD
}

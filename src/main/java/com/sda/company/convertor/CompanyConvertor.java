package com.sda.company.convertor;

import com.sda.company.dto.companydto.CompanyCreateDto;
import com.sda.company.dto.companydto.CompanyDisplayDto;
import com.sda.company.dto.companydto.CompanySummaryDto;
import com.sda.company.dto.companydto.CompanyUpdateDto;
import com.sda.company.model.Company;

public class CompanyConvertor {

    public static Company createDtoToEntity(CompanyCreateDto companyCreateDto) {
        Company company = new Company();

        company.setName(companyCreateDto.getName());
        company.setAddress(companyCreateDto.getAddress());
        company.setRegistrationNumber(companyCreateDto.getRegistrationNumber());
        company.setPhoneNumber(companyCreateDto.getPhoneNumber());
        company.setEmail(companyCreateDto.getEmail());

        return company;
    }

    public static CompanyDisplayDto entityToDisplayDto(Company company) {
        CompanyDisplayDto companyDisplayDto = new CompanyDisplayDto();

        companyDisplayDto.setId(company.getId());
        companyDisplayDto.setName(company.getName());
        companyDisplayDto.setAddress(company.getAddress());
        companyDisplayDto.setRegistrationNumber(company.getRegistrationNumber());
        companyDisplayDto.setPhoneNumber(company.getPhoneNumber());
        companyDisplayDto.setEmail(company.getEmail());

        return companyDisplayDto;
    }

    public static CompanySummaryDto entityToSummaryDto(Company company) {
        CompanySummaryDto companySummaryDto = new CompanySummaryDto();

        companySummaryDto.setId(company.getId());
        companySummaryDto.setName(company.getName());
        companySummaryDto.setRegistrationNumber(company.getRegistrationNumber());

        return companySummaryDto;
    }

    public static Company updateDtoToEntity(CompanyUpdateDto companyUpdateDto) {
        Company company = new Company();

        company.setId(companyUpdateDto.getId());
        company.setName(companyUpdateDto.getName());
        company.setRegistrationNumber(companyUpdateDto.getRegistrationNumber());
        company.setAddress(companyUpdateDto.getAddress());
        company.setEmail(companyUpdateDto.getEmail());
        company.setPhoneNumber(companyUpdateDto.getPhoneNumber());

        return company;
    }
}

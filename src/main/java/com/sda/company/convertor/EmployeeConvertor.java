package com.sda.company.convertor;

import com.sda.company.dto.employeDto.EmployeeCreateDto;
import com.sda.company.dto.employeDto.EmployeeDisplayDto;
import com.sda.company.dto.employeDto.EmployeeSummaryDto;
import com.sda.company.dto.employeDto.EmployeeUpdateDto;
import com.sda.company.model.employe.Employee;

public class EmployeeConvertor {

    public static Employee createDtoToEntity(EmployeeCreateDto employeeCreateDto) {
        Employee employee = new Employee();

        employee.setName(employeeCreateDto.getName());
        employee.setCnp(employeeCreateDto.getCnp());
        employee.setAddress(employeeCreateDto.getAddress());
        employee.setEmail(employeeCreateDto.getEmail());
        employee.setJobTitle(employeeCreateDto.getJobTitle());
        employee.setPhoneNumber(employeeCreateDto.getPhoneNumber());
        employee.setSalary(employeeCreateDto.getSalary());
        employee.setCompany(employeeCreateDto.getCompany());

        return employee;
    }

    public static EmployeeDisplayDto entityToDisplayDto(Employee employee) {
        EmployeeDisplayDto employeeDisplayDto = new EmployeeDisplayDto();

        employeeDisplayDto.setEmployeeId(employee.getEmployeeId());
        employeeDisplayDto.setAddress(employee.getAddress());
        employeeDisplayDto.setCnp(employee.getCnp());
        employeeDisplayDto.setEmail(employee.getEmail());
        employeeDisplayDto.setName(employee.getName());
        employeeDisplayDto.setJobTitle(employee.getJobTitle());
        employeeDisplayDto.setSalary(employee.getSalary());
        employeeDisplayDto.setCompany(employee.getCompany());
        employeeDisplayDto.setPhoneNumber(employee.getPhoneNumber());

        return employeeDisplayDto;
    }

    public static EmployeeSummaryDto entityToSummaryDto(Employee employee) {
        EmployeeSummaryDto employeeSummaryDto = new EmployeeSummaryDto();

        employeeSummaryDto.setEmployeeId(employee.getEmployeeId());
        employeeSummaryDto.setEmail(employee.getEmail());
        employeeSummaryDto.setName(employee.getName());
        employeeSummaryDto.setPhoneNumber(employee.getPhoneNumber());

        return employeeSummaryDto;
    }

    public static Employee updateDtoToEntity(EmployeeUpdateDto employeeUpdateDto) {
        Employee employee = new Employee();

        employee.setEmployeeId(employeeUpdateDto.getEmployeeId());
        employee.setName(employeeUpdateDto.getName());
        employee.setPhoneNumber(employeeUpdateDto.getPhoneNumber());
        employee.setCnp(employeeUpdateDto.getCnp());
        employee.setEmail(employeeUpdateDto.getEmail());
        employee.setAddress(employeeUpdateDto.getAddress());
        employee.setJobTitle(employeeUpdateDto.getJobTitle());
        employee.setSalary(employeeUpdateDto.getSalary());
        employee.setCompany(employeeUpdateDto.getCompany());

        return employee;
    }

    public static EmployeeUpdateDto entityToUpdateDto(Employee employee) {
        EmployeeUpdateDto employeeUpdateDto = new EmployeeUpdateDto();

        employeeUpdateDto.setEmployeeId(employee.getEmployeeId());
        employeeUpdateDto.setName(employee.getName());
        employeeUpdateDto.setPhoneNumber(employee.getPhoneNumber());
        employeeUpdateDto.setCnp(employee.getCnp());
        employeeUpdateDto.setEmail(employee.getEmail());
        employeeUpdateDto.setAddress(employee.getAddress());
        employeeUpdateDto.setJobTitle(employee.getJobTitle());
        employeeUpdateDto.setSalary(employee.getSalary());
        employeeUpdateDto.setCompany(employee.getCompany());

        return employeeUpdateDto;
    }

}

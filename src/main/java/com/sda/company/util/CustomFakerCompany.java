package com.sda.company.util;

import com.github.javafaker.Faker;
import com.sda.company.model.Company;

import java.util.ArrayList;
import java.util.List;

public class CustomFakerCompany {

    public List<Company> generateCompanies() {
        List<Company> companyList = new ArrayList<>();
        Faker faker = new Faker();

        for (int i = 0; i < 100; i++) {
            Company company = new Company();
            company.setName(faker.company().name());
            company.setPhoneNumber(faker.phoneNumber().phoneNumber());
            company.setEmail(faker.bothify("?????##@company.com"));
            company.setAddress(faker.address().fullAddress());
            company.setRegistrationNumber(faker.number().randomNumber(5, true));

            companyList.add(company);
        }

        return companyList;
    }
}

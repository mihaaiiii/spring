package com.sda.company.exceptions;

public class EmployeeException extends RuntimeException {

    public EmployeeException(String message) {
        super(message);
    }
}

package com.sda.company.controller;

import com.sda.company.exceptions.CompanyException;
import com.sda.company.exceptions.EmployeeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CompanyException.class)
    public ResponseEntity<Object> handlerCompanyException(CompanyException companyException) {
        Map<String, Object> response = new HashMap<>();
        response.put("Timestamp", LocalDateTime.now());
        response.put("Error message", companyException.getLocalizedMessage());

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EmployeeException.class)
    public ResponseEntity<Object> handlerEmployeeException(EmployeeException employeeException) {
        Map<String, Object> response = new HashMap<>();
        response.put("Timestamp", LocalDateTime.now());
        response.put("Error message", employeeException.getLocalizedMessage());

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
}

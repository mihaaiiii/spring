package com.sda.company.controller;

import com.sda.company.convertor.EmployeeConvertor;
import com.sda.company.dto.companydto.CompanyCreateDto;
import com.sda.company.dto.companydto.CompanyDisplayDto;
import com.sda.company.dto.companydto.CompanySummaryDto;
import com.sda.company.dto.companydto.CompanyUpdateDto;
import com.sda.company.dto.employeDto.EmployeeCreateDto;
import com.sda.company.dto.employeDto.EmployeeDisplayDto;
import com.sda.company.dto.employeDto.EmployeeUpdateDto;
import com.sda.company.model.Company;
import com.sda.company.model.employe.Employee;
import com.sda.company.model.employe.JobTitleEnum;
import com.sda.company.service.CompanyService;
import com.sda.company.service.impl.EmployeeService;
import com.sda.company.util.CustomFakerCompany;
import com.sda.company.util.CustomFakerEmployee;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/company")
@ControllerAdvice
public class CompanyController {

    private final CompanyService companyService;
    private final CustomFakerCompany customFakerCompany;
    private final CustomFakerEmployee customFakerEmployee;
    private final EmployeeService employeeService;

    @Autowired
    public CompanyController(CompanyService companyService, CustomFakerCompany customFakerCompany, CustomFakerEmployee customFakerEmployee, EmployeeService employeeService) {
        this.companyService = companyService;
        this.customFakerCompany = customFakerCompany;
        this.customFakerEmployee = customFakerEmployee;
        this.employeeService = employeeService;
    }

    @GetMapping("/hello")
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok("Hello world!");
    }

    @GetMapping("/hello/{name}")
    public ResponseEntity<String> helloName(@PathVariable String name, Integer age) {
        return ResponseEntity.ok("Hello " + name);
    }

    @GetMapping("/helloNameAndAge/{name}")
    public ResponseEntity<String> helloNameAndAge(@PathVariable String name, @RequestParam Integer age,
                                                  @RequestParam String gender) {
        return ResponseEntity.ok("Hello " + name + " age: " + age + " gender: " + gender);
    }

    @PostMapping("/create")
    public ResponseEntity<CompanyDisplayDto> createCompany(@RequestBody @Valid CompanyCreateDto companyCreateDto) {
        return ResponseEntity.ok(companyService.createCompany(companyCreateDto));
    }

    @GetMapping("/findByName/{companyName}")
    public ResponseEntity<CompanyDisplayDto> findByName(@PathVariable String companyName) {
        return ResponseEntity.ok(companyService.findByName(companyName));
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<CompanySummaryDto>> findAll(@RequestParam(defaultValue = "0") Integer pageNumber,
                                                           @RequestParam(defaultValue = "10") Integer pageSize,
                                                           @RequestParam(defaultValue = "id") String sortBy) {
        return ResponseEntity.ok(companyService.findAll(pageNumber, pageSize, sortBy));
    }

    @GetMapping("/generateCompanies")
    public ResponseEntity<String> generateCompanies() {
        companyService.saveAll(customFakerCompany.generateCompanies());
        return ResponseEntity.ok("Companies were generated!");
    }

    @PutMapping("/update")
    public ResponseEntity<CompanyDisplayDto> updateCompany(@RequestBody @Valid CompanyUpdateDto companyUpdateDto) {
        CompanyDisplayDto companyDisplayDto = companyService.updateCompany(companyUpdateDto);

        return ResponseEntity.ok(companyDisplayDto);
    }

    @DeleteMapping("/deleteById/{id}")
    public ResponseEntity<String> deleteCompany(@PathVariable Integer id) {
        companyService.deleteById(id);

        return ResponseEntity.ok("Company deleted successfully!");
    }

    @GetMapping("/generateEmployees")
    public ResponseEntity<String> generateEmployees() {
        employeeService.saveAll(customFakerEmployee.generateEmployees());
        return ResponseEntity.ok("Employees generated successfully!");
    }

    @PostMapping("/createEmployee")
    public ResponseEntity<EmployeeDisplayDto> createEmployee(@RequestBody @Valid
                                                             EmployeeCreateDto employeeCreateDto) {
        return ResponseEntity.ok(employeeService.createEmployee(employeeCreateDto));
    }

    @PutMapping("/updateEmployee")
    public ResponseEntity<EmployeeDisplayDto> updateEmployee(@RequestBody @Valid EmployeeUpdateDto employeeUpdateDto) {
        EmployeeDisplayDto employeeDisplayDto = employeeService.updateEmployee(employeeUpdateDto);

        return ResponseEntity.ok(employeeDisplayDto);
    }

    @GetMapping("/hireEmployee/{employeeName}&{companyName}&{salary}&{jobTitle}")
    public ResponseEntity<EmployeeDisplayDto> hireEmployee(@PathVariable String employeeName,
                                               @PathVariable String companyName,
                                               @PathVariable Long salary,
                                               @PathVariable JobTitleEnum jobTitle) {

        Employee employee = employeeService.findEmployeeByName(employeeName);
        Company company = companyService.findCompanyByName(companyName);

        employee.setCompany(company);
        employee.setSalary(salary);
        employee.setJobTitle(jobTitle);

        updateEmployee(EmployeeConvertor.entityToUpdateDto(employee));

        return ResponseEntity.ok(EmployeeConvertor.entityToDisplayDto(employee));
    }

    @GetMapping("/fireEmployee/{employeeName}")
    public ResponseEntity<EmployeeDisplayDto> fireEmployee(@PathVariable String employeeName) {
        Employee employee = employeeService.findEmployeeByName(employeeName);
        employee.setJobTitle(null);
        employee.setSalary(null);
        employee.setCompany(null);

        updateEmployee(EmployeeConvertor.entityToUpdateDto(employee));

        return ResponseEntity.ok(EmployeeConvertor.entityToDisplayDto(employee));
    }


}

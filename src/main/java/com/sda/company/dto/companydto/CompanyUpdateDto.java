package com.sda.company.dto.companydto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class CompanyUpdateDto {

    @NotNull(message = "Id must not be null!")
    private Integer id;

    @NotBlank(message = "Company name is mandatory!")
    private String name;

    @NotNull(message = "Registration number is mandatory!")
    @Min(value = 99, message = "The registration number should be greater than 98!")
    private Long registrationNumber;

    @NotBlank(message = "Address is mandatory!")
    private String address;

    @NotBlank(message = "Email is mandatory!")
    @Email(message = "Email is not valid!")
    private String email;

    @NotBlank(message = "Phone number is mandatory!")
    private String phoneNumber;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(Long registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
